<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
    	DB::table('users')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');

        User::create(
        [
            'dni' => 12345,
            'name' => 'Admin',
            'email' => 'a@gmail.com',
            'password' => bcrypt(12345),
            'remember_token' => bcrypt('restauracion')
        ]);   
    }
}
