<?php

use Illuminate\Database\Seeder;
use App\Models\debts;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class debtsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
    	DB::table('debts')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
        
        $debts = ['SmartTV', 'Colegiatura'];
        $amount = [100000000,5000000];

        for ($i=0; $i < sizeof($debts); $i++) { 
            debts::create(
            [
                'concept' => $debts[$i],
                'amount' => $amount[$i],
                'amount_paid' => 0,
                'note' => $debts[$i],
                'owner' => DB::table('users')->where('id', 1)->value('dni')
            ]);   
        }
    }
}
