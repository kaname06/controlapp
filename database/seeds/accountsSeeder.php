<?php

use Illuminate\Database\Seeder;
use App\Models\sub_bags;
use Illuminate\Support\Facades\DB;

class accountsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
    	DB::table('sub_bags')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
        
        $accounts = ['Wallet'];
        $percents = [100];

        for ($i=0; $i < sizeof($accounts); $i++) { 
            sub_bags::create(
            [
                'concept' => $accounts[$i],
                'percent' => $percents[$i],
                'amount' => 0,        	
                'note' => 'Dinero total en bolsillo',
                'bag_id' => DB::table('bags')
                                ->where('id', 1)
                                ->value('id')
            ]);   
        }
    }
}
