<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\bag;

class bagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
    	DB::table('bags')->truncate();
    	DB::statement('SET FOREIGN_KEY_CHECKS=1');

        bag::create(
        [
        	'id' => '1',
        	'amount' => 0,        	
        	'owner' => DB::table('users')
                           ->select('dni')
                           ->where('id', 1)
                           ->value('dni')
        ]);
    }
}
