<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDebtsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('debts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('concept');
            $table->bigInteger('amount')->default(0);
            $table->bigInteger('amount_paid')->default(0);
            $table->text('note');
            $table->string('owner', 30);
            $table->foreign('owner')->references('dni')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropForeign('owner');
        Schema::dropColumn('owner');
        Schema::dropIfExists('debts');
    }
}
