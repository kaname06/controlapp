<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubBagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_bags', function (Blueprint $table) {
            $table->increments('id');
            $table->string('concept');
            $table->bigInteger('amount')->default(0);
            $table->double('percent')->default(0);
            $table->text('note')->nullable();
            $table->unsignedInteger('bag_id');
            $table->foreign('bag_id')->references('id')->on('bags')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropForeign('bag_id');
        Schema::dropColumn('bag_id');
        Schema::dropIfExists('sub_bags');
    }
}
