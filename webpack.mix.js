const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.disableNotifications();
mix.js(
[  
   'node_modules/sweetalert2/dist/sweetalert2.all.js',
   'resources/js/app.js',
   'resources/js/utilities.js'
], 'public/js/app.js').version();

mix.styles(
[
   'public/css/linearicons.css',
   'public/icon/css/font-awesome.min.css',
   'public/css/animate.css',
   'public/css/fonts.css',
   'public/css/nice-select.css',   
], 'public/css/app_.css').version();  

mix.sass('resources/sass/app.scss', 'public/css/app.css').version();
