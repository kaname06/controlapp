<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () 
{
    return view('welcome');
});

Route::group(['middleware' => 'auth'], function()
{
	Route::get('/home', 'HomeController@index')->name('home');
	Route::post('/bag', 'BagController@BagDetails');
	Route::get('/bags', 'BagController@index');	
	Route::post('/accountslist', 'SubBagsController@index');
	Route::post('/upAccount', 'SubBagsController@Update');
	Route::post('/cashin' , 'SubBagsController@CashIn');
	Route::post('/listMov' , 'MovementsController@index');
	Route::post('/newMov' , 'MovementsController@store');
	Route::post('/debtslist', 'DebtsController@index');
	Route::post('/paydebt', 'DebtsController@payDebt');
	Route::post('/newDebtMov', 'DebtMovementsController@store');
	Route::post('/debtHistory', 'DebtsController@history');
	Route::post('/filterMovs', 'MovementsController@filter');
	Route::post('/walletIn', 'SubBagsController@walletIn');
	Route::post('/debt', 'DebtsController@Debt');
	Route::post('/account', 'SubBagsController@Account');

	Route::group(['prefix' => 'register'], function()
	{
		Route::post('/newAccount', 'SubBagsController@register');
		Route::post('/editAccount', 'SubBagsController@Edit');
		Route::post('/deleteAccount', 'SubBagsController@Delete');
		Route::post('/newDebt', 'DebtsController@register');
		Route::post('/editDebt', 'DebtsController@Edit');
		Route::post('/deleteDebt', 'DebtsController@Delete');
		Route::post('/bag', 'BagController@registerBag');
	});
});

Route::post('/login', 'LoginController@login')->name('log');
Auth::routes();



