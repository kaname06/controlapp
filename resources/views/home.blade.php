@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-6 col-md-6 col-sm-12 px-1 mb-3">
            <div class="col-lg-12 col-md-12 col-sm-12 px-1">
                <bag-component></bag-component>            
            </div>
            <div class="col-12">
                <hr>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 px-1">
                <debt-component></debt-component>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 px-1">
            <div class="col-lg-12 col-md-12 col-sm-12 px-1">
                <account-component></account-component>        
            </div>
            <div class="col-12">
                <hr>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 px-1">
                <moves-component></moves-component>
            </div>
        </div>
    </div>
</div>
@endsection
