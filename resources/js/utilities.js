$(document).ready(function()
{   
    setTimeout(function(){$('[data-toggle="tooltip"]').tooltip();}, 1000)
    setTimeout(function(){$('[data-toggle="popover"]').popover()}, 1000)    
    var newDate = new Date();     
    let dateString = '';
    dateString += newDate.getFullYear() + "-";
    dateString += (newDate.getMonth() + 1) + "-";
    dateString += newDate.getDate();  
    setTimeout(function(){$('input[type=date]').val(dateString)}, 1000)
})