require('./bootstrap');

window.Vue = require('vue');
require('./plugins/sweet-alert-plugin');

// var bus = new Vue();

Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('bag-component', require('./components/bagComponent.vue'));
Vue.component('debt-component', require('./components/DebtsComponent.vue'));
Vue.component('moves-component', require('./components/MovementsComponent.vue'));
Vue.component('account-component', require('./components/AccountComponent.vue'));


// import swalPlugin from 'sweetalert2';
// Vue.use(swalPlugin);

const app = new Vue(
{
    el: '#app'
});


$('[data-toggle="tooltip"]').tooltip()
