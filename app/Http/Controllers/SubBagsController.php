<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\sub_bags;
use Illuminate\Support\Facades\DB;

class SubBagsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $accounts = sub_bags::select('id','concept as name', 'amount', 'percent', 'note')->where('bag_id', 1)->orderBy('id', 'DESC')->get();
        $i=0;
        foreach ($accounts as $key => $value) 
        {
            $accounts[$i]->stateAc = false;
            $accounts[$i]->checked = false;
            $accounts[$i]->amount_formated = number_format($accounts[$i]->amount, 0, ' ', '.');
            $i++;
        }
        return $accounts;
    }

    public function Update(Request $axios)
    {
        $ac = sub_bags::find($axios->id);
        $ac->amount = (int) $axios->actual - (int) $axios->retiro;
        $ac->save();
        return $ac;
    }

    public function CashIn(Request $axios)
    {
        $ac = sub_bags::find($axios->id);
        $ac->amount += ($axios->cash*$ac->percent)/100;
        $ac->save();
        return $ac;
    }

    public function register(Request $axios)
    {
        $ax = $axios->all();        
        $request = sub_bags::create(
        [
            'concept' => $ax['params']['name'],
            'amount' => 0,
            'percent' => $ax['params']['percent'],
            'note' => $ax['params']['note'],
            'bag_id' => 1
        ]);

        if($request)        
            return 'true';
        else
            return 'false';        
    }

    public function walletIn(Request $axios)
    {
        $data = sub_bags::where('concept', 'Wallet')->value('id');
        $percent = 0;
        for($i = 0; $i < sizeof($axios->accounts); $i++)
        {
            $per = DB::table('sub_bags')->where('id', $axios->accounts[$i])->value('percent');
            $percent += $per;
        }
        $cash = $axios->cash - (($axios->cash * $percent)/ 100);
        $query = sub_bags::find($data);
        $query->amount += $cash;
        $query->save();

        return $query;
    }

    public function Account(Request $axios)
    {
        $query = sub_bags::find($axios->account);
        return $query;
    }

    public function Edit(Request $axios)
    {
        $data = $axios->all();
        $query = sub_bags::find($data['params']['account']);
        if($data['params']['wallet'])
        {
            $id = sub_bags::where('concept', 'Wallet')->value('id');
            $wallet = sub_bags::find($id);
            $wallet->amount += $query->amount;
            $wallet->save();
        }
        if($data['params']['name'] != null)
        {
            $query->concept = $data['params']['name'];
        }
        if($data['params']['note'] != null)
        {
            $query->note = $data['params']['note'];
        }
        $query->percent = $data['params']['percent'];
        $query->amount = 0;
        $query->save();
        return $query;
    }

    public function Delete(Request $axios)
    {
        $query = sub_bags::find($axios->account);
        $query->delete();
    }
}
