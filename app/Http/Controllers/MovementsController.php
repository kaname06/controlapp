<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\movements;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class MovementsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user = Auth::user()->dni;
        // $moves = movements::select('id', 'amount', 'debt_id as debt', 'type as action', 'created_at as date')
        // ->where([['bag_id', 1],['']])->orWhere('debt_id', '<>', null)->orderByDesc('id')->take(10)->get();

        $query = DB::select('SELECT movements.amount as amount, movements.type as actionn, movements.id as id, movements.created_at as dat, movements.debt_id as debt FROM movements, bags, debts WHERE (movements.bag_id = bags.id AND bags.owner = ?) OR (movements.debt_id = debts.id AND debts.owner = ?) GROUP BY movements.id ORDER BY id DESC LIMIT 10', [$user, $user]);
        $i=0;
        foreach ($query as $key => $value) 
        {
            $query[$i]->debt = DB::table('debts')->where('id', $query[$i]->debt)->value('concept');
            $query[$i]->amount = number_format($query[$i]->amount, 0, ' ', '.');
            $i++;
        }
        return $query;
    }

    public function store(Request $axios)
    {
        $types = ['Retirado', 'Consignado', 'Pagado'];
        if($axios->type == 'retiro'){$val = $types[0]; $bag = 1; $debt=null;}
        elseif($axios->type == 'consignacion'){$val = $types[1]; $bag = 1; $debt=null;}
        elseif($axios->type == 'pago'){$val = $types[2];$bag = null; $debt=$axios->debt;}
        $mov = movements::create([
            'bag_id' => $bag,
            'debt_id' => $debt,
            'amount' => $axios->amount,
            'type' => $val
        ]);
        
        if ($mov) 
        {
            $deb = DB::table('debts')->where('id', $debt)->value('concept');
            $date = date_create($mov->created_at);
            $res = date_format($date, 'Y-m-d H:i:s');
            $visualCash = number_format($axios->amount, 0, ' ', '.');
            $response = ['actionn' => $val, 'amount' => $visualCash, 'dat' => $res, 'debt' => $deb];
            return $response;
        }
        else 
        {
            return 'error';
        }
    }

    public function filter(Request $axios)
    {
        $user = Auth::user()->dni;
        // $moves = movements::select('id', 'amount', 'debt_id as debt', 'type as action', 'created_at as date')
        // ->where([['bag_id', 1],['']])->orWhere('debt_id', '<>', null)->orderByDesc('id')->take(10)->get();

        $date = date($axios->end);
        $ended = strtotime($date."+ 1 days");
        $end = date("Y-m-d",$ended);

        $query = DB::select('SELECT movements.amount as amount, movements.type as actionn, movements.id as id, movements.created_at as dat, movements.debt_id as debt FROM movements, bags, debts WHERE ((movements.bag_id = bags.id AND bags.owner = ?) OR (movements.debt_id = debts.id AND debts.owner = ?)) and movements.created_at BETWEEN ? AND ? GROUP BY movements.id ORDER BY id DESC', [$user, $user, $axios->start, $end]);
        $i=0;
        foreach ($query as $key => $value) {
            $query[$i]->debt = DB::table('debts')->where('id', $query[$i]->debt)->value('concept');
            $query[$i]->amount = number_format($query[$i]->amount, 0, ' ', '.');
            $i++;
        }
        return $query;
    }
}
