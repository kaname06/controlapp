<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\debts_movements;
use Illuminate\Support\Facades\DB;

class DebtMovementsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Request $axios)
    {
        $val = 'Pagado';
        $mov = debts_movements::create([
            'debt_id' => DB::table('debts')->where('id', $axios->debt)->value('id'),
            'pay' => $axios->amount,
        ]);
        
        if ($mov) 
        {
            $debt = DB::table('debts')->where('id', $axios->debt)->value('concept');
            $date = date_create($mov->created_at);
            $res = date_format($date, 'Y-m-d H:i:s');
            $response = ['action' => $val, 'amount' => $axios->amount, 'date' => $res, 'debt' => $debt];
            return $response;
        }
        else 
        {
            return 'error';
        }
    }
}
