<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\bag;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class BagController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth');
    }

    function bagDetails(Request $axios)
    {
        if ($axios->ajax()) 
        {
            $data = bag::find(1)->value('amount');            
            if($data == null)
            {
                $data = 0;
            }
            // $data1 = number_format((int) $data, 0, ',', '.');
            return $data;
        }
    }

    function index()
    {
        $bag = bag::all();
        return $bag;
    }

    function registerBag(Request $axios)
    {
        if ($axios->ajax()) 
        {
            $req = $axios->all();
            // dd($req['params']['cash']);
            $updateCash = bag::find(1);
            $updateCash->amount += $req['params']['cash'];
            $query = $updateCash->save();          
            if($query)        
                return response()->json(['state' => true, 'amount' => (int)$req['params']['cash']]); 
            else
                return response()->json(['state' => false]);
        }
    }
}
