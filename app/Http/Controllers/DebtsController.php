<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\debts;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DebtsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $debts = 
        debts::select('id', 'concept', 'amount', 'amount_paid as paid', 'note')
        ->where('owner', Auth::user()->dni)        
        ->orderBy('id', 'DESC')
        ->get();
        $i=0;
        foreach ($debts as $key => $value) 
        {
            if($debts[$i]->amount == $debts[$i]->paid)
            {
                $debts[$i]->isPaid = true;
            }
            else
            {
                $debts[$i]->isPaid = false;
            }
            $debts[$i]->debt = $debts[$i]->amount - $debts[$i]->paid;
            $debts[$i]->statePay = false;
            $debts[$i]->debt = number_format($debts[$i]->debt, 0, ' ', '.');
            $debts[$i]->paid = number_format($debts[$i]->paid, 0, ' ', '.');
            $i++;
        }

        return $debts;
    }

    public function isPayed($id)
    {
        $debts = 
        debts::select('id','amount', 'amount_paid as paid')
        ->where('owner', Auth::user()->dni) 
        ->where('id', $id)       
        ->orderBy('id', 'DESC')
        ->get();
        $i=0;
        $paid = false;
        foreach ($debts as $key => $value) 
        {
            if($debts[$i]->amount == $debts[$i]->paid)
            {
                $paid = true;
            }
            else
            {
                $paid = false;
            }
            $i++;
        }
        return $paid;
    }

    public function payDebt(Request $axios)
    {
        $debt = debts::find($axios->id);
        $debt->amount_paid += $axios->amount;
        $debt->save();

        return $debt;
    }

    public function history(Request $axios)
    {
        $paid = $this->isPayed($axios->debt);
        $debts = DB::table('movements')->where('debt_id', $axios->debt)->get();
        $response = [];
        $i=0;
        $vec =  array('Ene' , 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic');    
        foreach ($debts as $key => $value) 
        {
            $d = date_create($debts[$i]->created_at);
            // $d = date_format($d, 'Y-m-d')
            $mes = $vec[date_format($d, 'n') - 1];
            $dia = date_format($d, 'd');
            $año = date_format($d, 'Y');            
            $data = array(
                'action' => $debts[$i]->type,
                'amount' => $debts[$i]->amount,
                'date' => $dia.'-'.$mes.'-'.$año
            );
            $response[] = $data;
            $data = null;
            $i++;
        }
        $response[$i]['paid'] = $paid;
        return $response;
    }

    public function register(Request $axios)
    {
        $ax = $axios->all();        
        $request = debts::create(
        [
            'concept' => $ax['params']['name'],
            'amount' => $ax['params']['pay'],    
            'amount_paid' => 0,        
            'note' => $ax['params']['note'],
            'owner' => Auth::user()->dni
        ]);

        if($request)        
            return 'true';
        else
            return 'false';        
    }

    public function Debt(Request $axios)
    {
        $query = debts::find($axios->debt);
        return $query;
    }

    public function Edit(Request $axios)
    {
        $data = $axios->all();
        $query = debts::find($data['params']['debt']);
        if($data['params']['name'] != null)
        {
            $query->concept = $data['params']['name'];
        }
        if($data['params']['note'] != null)
        {
            $query->note = $data['params']['note'];
        }
        $query->amount = $data['params']['pay'];
        $query->save();
        // if($query)
        // {
        //     return 'true';
        // }
        // else
        // {
        //     return 'false';
        // }
        return $query;
    }
    public function Delete(Request $axios)
    {
        $debt = debts::find($axios->debt);
        $debt->delete();
        if($debt)
        {
            return 'Eliminado';
        }
    }
}
