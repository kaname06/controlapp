<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $ajax)
    {
        $request = $ajax->all();
        $remember = false;
        // dd($request);
        if ($ajax->has('remember'))
        {
          $remember = $request['remember'];
          if($remember == 'false')
          {
            $remember = false;
          }else
          {
            $remember = true;
          }
        }

        if(Auth::attempt(['dni' => $request["dni"], 'password' => $request['password']], $remember))
        {
            return redirect('/home');
        }
        else
        {
          $url = '/login';
           return redirect($url)->withErrors(['AttempFail'=>'Estas credenciales no fueron encontradas en el servidor...']);
        }
     }
}
