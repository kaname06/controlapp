<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class sub_bags extends Model
{
  protected $table = "sub_bags";

  protected $fillable =
  [
    'amount', 'bag_id', 'id', 'percent', 'concept', 'note'
  ];
}
