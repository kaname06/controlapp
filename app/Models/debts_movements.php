<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class debts_movements extends Model
{
  protected $table = "debts_movements";

  protected $fillable =
  [
    'pay', 'debt_id', 'id'
  ];
}
