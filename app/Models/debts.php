<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class debts extends Model
{
    protected $table = "debts";

    protected $fillable =
    [
      'concept', 'note', 'id', 'owner', 'amount', 'amount_paied'
    ];
}
