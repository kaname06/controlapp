<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class bag extends Model
{
  protected $table = "bags";

  protected $fillable =
  [
    'amount', 'owner', 'id'
  ];
}
