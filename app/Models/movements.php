<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class movements extends Model
{
    protected $table = "movements";

    protected $fillable =
    [
      'amount', 'bag_id', 'debt_id', 'type'
    ];
}
